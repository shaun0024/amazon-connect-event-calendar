# AMAZON CONNECT EVENT CALENDAR

## Description
This solution was developed as an add-on to overcome some of the limitations when using Amazon Connect. Out-of-the-box, Amazon Connect only has a standard business hour schedule that is organised by days. As this is just a schedule and not a calendar, it is static, making it inconvenient to cater for non-standard schedules such as public holidays.

With this add-on, an event calendar can be used with Amazon Connect that supports:
- synchronisation with selected public Google Calendars using the Google Calendar API automatically
- adding custom events
- integrating with different timezones
- integrating with different queues



## Solution Overview
The diagram below is a high level illustration of the solution design.

![Architecture](docs/imgs/high-level-architecture.png)

The following is a description of the actions taken during each step:

1. Based on a defined schedule (every Sunday at 00:00 by default), the ```Sync Events Lambda``` is invoked.
2. The ```Sync Events Lambda``` queries the ```Google Calendar API```.
3. The response from the API is stored in an ```DynamoDB Event Table```.
4. The ```Get Event Lambda``` is added to Amazon Connect and built into contact flows for invocation.
5. The ```Get Event Lambda``` checks the ```DynamoDB Event Table``` and returns a response to the contact flow.



## Frequently Asked Questions

1. **When would I need to associate multiple different timezones?**
This is a very open ended question and will vary. For example, if you are running a contact centre that has different parts of the operations running across different states (e.g. order processing in Sydney and support in Melbourne) and do not implement a standardised schedule, you may want to consider associating the calendar to multiple different timezones.

2. **Can I add my own events?**
Yes, while this automatically synchronises events with the selected public calendars using the Google Calendar API, you can also add your own events. Refer to the [usage guide](docs/02-usage.md).

3. **How are multi-day events handled?**
The public calendars provided by Google Calendar, generally speaking, splits each event into a different entry (e.g. 'Event (Day 1)', 'Event (Day 2)'). If you are adding your own multi-day events, create a separate entry for each day of the event. Refer to the [usage guide](docs/02-usage.md).

4. **Can I associate the event only to specific queues?**
Yes, you can associate the event to only specific queues. Refer to the [usage guide](docs/02-usage.md).



## Deployment Guide
Please refer to [deployment guide](docs/01-deployment.md).