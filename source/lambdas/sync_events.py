"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
import json
import pytz
import requests
import urllib.parse
from boto3 import client, resource
from datetime import datetime, timedelta

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Global variables"""
table = environ['TABLE_NAME']
secret_arn = environ['SECRET_ARN']
ssm_parameter_name = environ['SSM_PARAMETER_NAME']
sync_ahead_weeks = 24 if 'SYNC_AHEAD_WEEKS' not in environ else int(environ['SYNC_AHEAD_WEEKS'])
expire_after_weeks = 4 if 'EXPIRE_AFTER_WEEKS' not in environ else int(environ['EXPIRE_AFTER_WEEKS'])
api_base_url = 'https://www.googleapis.com'
api_path = 'calendar/v3/calendars'
api_version = 'v3'

"""Global clients"""
secret = client('secretsmanager')
ssm = client('ssm')
dynamo = resource('dynamodb').Table(table)



def lambda_handler(event, context):
    """The entrypoint for the Lambda.

    Parameters:
    ----------
    - event [dictionary]: The event received from CloudFormation.
    - context [dictionary]: The context of the event received from CloudFormation.

    Return:
    ------
    None
    """

    logger.debug(f'Received event: {event}')

    controller()

    return



def controller():
    """The router that iterates through the response from the Google Calendar API, transforms them and updates the database.

    Parameters:
    ----------
    None

    Return:
    ------
    None
    """

    api_key = get_api_key()
    calendar_map = get_calendar_map()
    current_time = datetime.now(pytz.utc)
    end_sync_period_epoch = (current_time + timedelta(weeks = sync_ahead_weeks)).strftime('%s')

    events = []
    for index, calendar_id in enumerate(calendar_map):
        url = f"{api_base_url}/{api_path}/{urllib.parse.quote(calendar_id)}/events?key={api_key}"
        response = requests.get(url)

        if response.status_code == 200:        
            for timezone in calendar_map[calendar_id]:
                events.extend(format_event(**{
                    'Items': response.json()['items'], 
                    'Timezone': timezone, 
                    'CurrentEpochTime': current_time.astimezone(pytz.timezone(timezone)).strftime('%s'), 
                    'EndSyncEpochTime': end_sync_period_epoch
                }))
            
    update_table(Events = events)

    return



def format_event(**kwargs):
    """Formats the response from the Google Calendar API into the format required.

    Parameters:
    ----------
    - Timezone [string]: The name of the timezone.
    - CurrentEpochTime [string]: Epoch time representing the current time.
    - EndSyncEpochTime [string]: Epoch time representing the end time for synchronisation.
    - Items [list of dictionaries]: List of dictionaries representing events from the Google Calendar API.
    
    Return:
    ------
    List of dictionaries
    """

    events = []
    for item in kwargs['Items']:
        start_time_epoch = datetime.strptime(item['start']['date'], '%Y-%m-%d').astimezone(pytz.timezone(kwargs['Timezone'])).strftime('%s')
        if start_time_epoch > kwargs['CurrentEpochTime'] and start_time_epoch < kwargs['EndSyncEpochTime']:
            for x in range((datetime.strptime(item['end']['date'], '%Y-%m-%d') - datetime.strptime(item['start']['date'], '%Y-%m-%d')).days):
                events.append({
                    'PKey': kwargs['Timezone'],
                    'SKey': f"{(datetime.strptime(item['start']['date'], '%Y-%m-%d') + timedelta(days = x)).strftime('%Y%m%d')}_{item['id'].split('_')[-1]}",
                    'EventName': item['summary'],
                    'EventDescription': item['description'],
                    'StartEpochTime': int(start_time_epoch),
                    'EndEpochTime': int(datetime.strptime(item['end']['date'], '%Y-%m-%d').astimezone(pytz.timezone(kwargs['Timezone'])).strftime('%s')),
                    'ExpirationEpochTime': int((datetime.strptime(item['end']['date'], '%Y-%m-%d').astimezone(pytz.timezone(kwargs['Timezone'])) + timedelta(weeks = 12)).strftime('%s')),
                    'Observed': False,
                    'DoNotUpdate': False
                })
                    
    return events



def update_table(**kwargs):
    """Writes the events into DynamoDB.

    Parameters:
    ----------
    - Events [list of dictionaries]: The name of the timezone.
    
    Return:
    ------
    None
    """

    def __get_item(**kwargs):
        """Writes the events into DynamoDB.

        Parameters:
        ----------
        - PKey [string]: The hash key of the DynamoDB item.
        - SKey [string]: The range key of the DynamoDB item.
        
        Return:
        ------
        None
        """

        params = {
            'TableName': table,
            'Key': {
                'PKey': kwargs['PKey'],
                'SKey': kwargs['SKey']
            }
        }

        result = dynamo.get_item(**params)

        if 'Item' in result:
            return result['Item']
        else:
            return None


    items = []
    for event in kwargs['Events']:
        existing_item = __get_item(**event)
        
        if existing_item is not None:
            if existing_item['DoNotUpdate'] is False:
                existing_item.update({
                    'EventName': event['EventName'],
                    'EventDescription': event['EventDescription'],
                    'StartEpochTime': event['StartEpochTime'],
                    'EndEpochTime': event['EndEpochTime']
                })
                items.append(existing_item)
        
        else:
            items.append(event)

    with dynamo.batch_writer() as batch:
        for item in items:
            batch.put_item(Item = item)



def get_api_key():
    """Gets the Google Calendar API key from the Secrets Manager.

    Parameters:
    ----------
    None
    
    Return:
    ------
    String
    """

    api_key = json.loads(secret.get_secret_value(SecretId = secret_arn)['SecretString'])['ApiKey']

    return api_key



def get_calendar_map():
    """Gets the calendar map from the Systems Manager Parameter Store.

    Parameters:
    ----------
    None
    
    Return:
    ------
    Dictionary
    """

    calendar_map = json.loads(ssm.get_parameter(Name = ssm_parameter_name)['Parameter']['Value'])

    return calendar_map