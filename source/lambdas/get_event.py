"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
import json
import pytz
from boto3 import resource
from datetime import datetime

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Global variables"""
table = environ['TABLE_NAME']

"""Global clients"""
dynamo = resource('dynamodb').Table(table)



def lambda_handler(event, context):
    """The entrypoint for the Lambda.

    Parameters:
    ----------
    - event [dictionary]: The event received from CloudFormation.
    - context [dictionary]: The context of the event received from CloudFormation.

    Return:
    ------
    None
    """

    logger.debug(f'Received event: {event}')

    response = controller(event)

    logger.debug(f'Return response: {response}')

    return response



def controller(event):
    timezone = event['Details']['Parameters']['Timezone']
    queue_name = event['Details']['Parameters'].get('QueueName', None)
    current_time = datetime.now(pytz.timezone(timezone))

    query = {
        'TableName': table,
        'KeyConditionExpression': 'PKey = :Timezone and begins_with(SKey, :CurrentDate)',
        'ExpressionAttributeValues': {
            ':Timezone': timezone,
            ':CurrentDate': current_time.strftime('%Y%m%d'),
            ':CurrentEpochTime': int(current_time.astimezone(pytz.utc).strftime('%s')),
            ':Observed': True
        },
        'FilterExpression': '(StartEpochTime < :CurrentEpochTime and EndEpochTime > :CurrentEpochTime and Observed = :Observed)'
    }

    if queue_name is not None:
        query['ExpressionAttributeValues'].update({ ':QueueName': queue_name })
        query['FilterExpression'] = f"{query['FilterExpression']} and (contains(AssociatedQueues, :QueueName))"

    logger.debug(f'Running query: {query}')

    response = dynamo.query(**query)

    logger.debug(response)

    if response['Count'] == 1:
        return {
            'Found': True,
            'EventName': response['Items'][0]['EventName'],
            'CustomPrompt': 'none' if 'CustomPrompt' not in response['Items'][0] else response['Items'][0]['CustomPrompt']
        }

    else:
        return {
            'Found': False
        }