AWSTemplateFormatVersion: 2010-09-09


Description: Deploys the contact center event calendar that synchronises with Google Calendar public events.


Parameters:
  LoggingLevel:
    Description: The logging level for the Lambda functions.
    Type: String
    AllowedValues:
      - INFO
      - ERROR
      - WARNING
      - DEBUG
    Default: INFO

  SyncAheadWeeks:
    Description: The number of weeks ahead to synchronize events.
    Type: Number
    Default: 24

  ExpireAfterWeeks:
    Description: The number of weeks to hold expired events.
    Type: Number
    Default: 12

  SecretArn:
    Description: The ARN of the secret in Secrets Manager containing the API key.
    Type: String

  CalendarMapName:
    Description: The name of the SSM parameter. Include '/' in the start of the name.
    Type: String

  CalendarMap:
    Description: JSON formatted string of the calendat-to-timezone map.
    Type: String


Resources:
  CalendarMapParameter:
    Type: AWS::SSM::Parameter
    Properties:
      Name: !Ref CalendarMapName
      Description: JSON formatted string of the calendat-to-timezone map.
      Type: String
      Value: !Ref CalendarMap

  EventDatabase:
    Type: AWS::DynamoDB::Table
    Properties:
      BillingMode: PAY_PER_REQUEST
      AttributeDefinitions:
        - AttributeName: PKey
          AttributeType: S
        - AttributeName: SKey
          AttributeType: S
      KeySchema:
        - AttributeName: PKey
          KeyType: HASH
        - AttributeName: SKey
          KeyType: RANGE
      TimeToLiveSpecification:
        AttributeName: ExpirationEpochTime
        Enabled: true
      SSESpecification:
        SSEEnabled: true
        SSEType: KMS

  EventCalendarRole:
    Type: AWS::IAM::Role
    Properties:
      Description: Used by the event management Lambdas.
      AssumeRolePolicyDocument:
        Statement:
          - Action: sts:AssumeRole
            Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
      Policies:
        - PolicyName: event-calendar-policy
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Action:
                  - dynamodb:GetItem
                  - dynamodb:Query
                  - dynamodb:BatchWriteItem
                  - dynamodb:PutItem
                  - dynamodb:UpdateItem
                Effect: Allow
                Resource:
                  - !GetAtt EventDatabase.Arn
              - Action:
                  - secretsmanager:GetSecretValue
                Effect: Allow
                Resource:
                  - !Ref SecretArn
              - Action:
                  - ssm:GetParameter
                Effect: Allow
                Resource:
                  - !Sub arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter${CalendarMapParameter}

  ShrdLmbdModules:
    Type: AWS::Lambda::LayerVersion
    Properties:
      Description: Layer containing Python modules used for Event Calendar lambdas
      LicenseInfo: MIT
      CompatibleRuntimes:
        - python3.9
      Content: ../lambda-layer/libs

  ShrdLmbdModulesPermission:
    Type: AWS::Lambda::LayerVersionPermission
    Properties:
      Action: lambda:GetLayerVersion
      LayerVersionArn: !Ref ShrdLmbdModules
      Principal: !Sub ${AWS::AccountId}

  EventSynchroniser:
    Type: AWS::Lambda::Function
    Properties:
      Description: Synchronises the Event Database with Google Calendar public calendars.
      Architectures:
        - arm64
      MemorySize: 128
      Role: !GetAtt EventCalendarRole.Arn
      Runtime: python3.9
      Handler: sync_events.lambda_handler
      Timeout: 120
      Layers:
        - !Ref ShrdLmbdModules
      Code: ../lambdas
      Environment:
        Variables:
          LOGGING_LEVEL: !Ref LoggingLevel
          TABLE_NAME: !Ref EventDatabase
          SECRET_ARN: !Ref SecretArn
          SSM_PARAMETER_NAME: !Ref CalendarMapName
          SYNC_AHEAD_WEEKS: !Ref SyncAheadWeeks
          EXPIRE_AFTER_WEEKS: !Ref ExpireAfterWeeks

  EventSynchroniserSchedule:
    Type: AWS::Events::Rule
    Properties:
      ScheduleExpression: cron(0 0 ? * SUN *)
      State: ENABLED
      Targets:
        - Id: !Sub ${AWS::StackName}-SyncCalendar
          Arn: !GetAtt EventSynchroniser.Arn

  EventSynchroniserPermission:
    Type: AWS::Lambda::Permission
    Properties:
      Action: lambda:InvokeFunction
      FunctionName: !GetAtt EventSynchroniser.Arn
      Principal: events.amazonaws.com
      SourceArn: !GetAtt EventSynchroniserSchedule.Arn

  CheckCurrentEvent:
    Type: AWS::Lambda::Function
    Properties:
      Description: Checks to see if there are any matching event in the current time.
      Architectures:
        - arm64
      MemorySize: 128
      Role: !GetAtt EventCalendarRole.Arn
      Runtime: python3.9
      Handler: get_event.lambda_handler
      Timeout: 10
      Layers:
        - !Ref ShrdLmbdModules
      Code: ../lambdas
      Environment:
        Variables:
          LOGGING_LEVEL: !Ref LoggingLevel
          TABLE_NAME: !Ref EventDatabase