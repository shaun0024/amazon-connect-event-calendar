# USAGE

## The Event Table

The Event Table uses the following schema.

| Attribute | Type | Required | Description |
|---|---|---|---|
| PKey | String | Yes | The name of the [timezone](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones). |
| SKey | String | Yes | This is the date of the event and must be in the format of ```{YYYYMMDD}_{unique_string}``` (e.g. 20230321_hg5adesh16s1d9nfb6ln7pfpos). |
| EventName | String | Yes | The name of the event (e.g. Christmas day). |
| EventDescription | String | Yes | The description of the event. |
| StartEpochTime | Integer | Yes | The [epoch time](https://www.epochconverter.com/timezones) for the start time of the event. |
| EndEpochTime | Integer | Yes | The [epoch time](https://www.epochconverter.com/timezones) for the end time of the event. |
| ExpirationEpochTime | Integer | Yes | The [epoch time](https://www.epochconverter.com/timezones) for when the event will be automatically deleted from the database. |
| Observed | Boolean | Yes | Determines if the event is observed (i.e. it will be found by the ```Check Current Event Lambda```). This is ```false``` by default. |
| DoNotUpdate | Boolean | Yes | Determines if the event is overwritten during the synchronisation. If this is set to ```true```, when the scheduled synchronisation takes place, any manual changes made to ```EventName```, ```EventDescription```, ```StartEpochTime``` and ```EndEpochTime``` will be maintained (i.e. not overwritten by results from the API). This is ```false``` by default. |
| CustomPrompt | String | No | A custom prompt that can be used in the contact flow. |
| AssociatedQueues | Array of Strings | No | A list of queue names that the event is associated with. |


## Updating the Events to Match Requirements

### Examples

**Standard event from Google Calendar (or custom event)**

Standard item of event when synchronised from Google Calendar.

```
{
    "PKey": "Australia/Melbourne",
    "SKey": "20230321_hg5adesh16s1d9nfb6ln7pfpos",
    "DoNotUpdate": false,
    "EndEpochTime": 1679482800,
    "EventDescription": "Observance\nTo hide observances, go to Google Calendar Settings > Holidays in Australia",
    "EventName": "Harmony Day",
    "ExpirationEpochTime": 1686740400,
    "Observed": false,
    "StartEpochTime": 1679396400
}
```

If you need to create your own custom event, follow the format above.

**Standard event from Google Calendar**

Standard item of event when synchronised from Google Calendar.

```
{
    "PKey": "Australia/Melbourne",
    "SKey": "20230321_hg5adesh16s1d9nfb6ln7pfpos",
    "DoNotUpdate": false,
    "EndEpochTime": 1679482800,
    "EventDescription": "Observance\nTo hide observances, go to Google Calendar Settings > Holidays in Australia",
    "EventName": "Harmony Day",
    "ExpirationEpochTime": 1686740400,
    "Observed": false,
    "StartEpochTime": 1679396400
}
```

**Select the events to be observed**

By default, when using the ```Check Current Event Lambda``` that is included, none of the events synchronised from Google Calendar are observed. What this means is that even though an event might exist, it will not be returned as the ```Observed``` attribute is set to ```false```.

To change this, update the ```Observed``` attribute to ```true```.

```
{
    "PKey": "Australia/Melbourne",
    "SKey": "20230407_oa8fkd8pfpv2h9vs4vsacr7094",
    "DoNotUpdate": false,
    "EndEpochTime": 1680948000,
    "EventDescription": "Public holiday in Victoria",
    "EventName": "Good Friday (Victoria)",
    "ExpirationEpochTime": 1688205600,
    "Observed": true,
    "StartEpochTime": 1680861600
}
```

**Lock the events**

You can modify the attributes ```EventName```, ```EventDescription```, ```StartEpochTime```, ```EndEpochTime``` to make it more meaningful compared to the default ones from the Google Calendar API.

However, by default, whenever a weekly synchronisation takes place, it will overwrite these values with the ones from Google Calendar. Note that custom events are not updated.

To prevent this, update the ```DoNotUpdate``` key to ```false```.

```
{
    "PKey": "Australia/Melbourne",
    "SKey": "20230407_oa8fkd8pfpv2h9vs4vsacr7094",
    "DoNotUpdate": true,
    "EndEpochTime": 1680948000,
    "EventDescription": "Public holiday across Australia",
    "EventName": "Good Friday",
    "ExpirationEpochTime": 1688205600,
    "Observed": true,
    "StartEpochTime": 1680861600
}
```

**Return a custom prompt**

To create a custom prompt that could be used by the contact flow for an automated response, add an attribute called ```CustomPrompt``` with a string representing the prompt that will be returned toether with the event.

```
{
    "PKey": "Australia/Melbourne",
    "SKey": "20230407_oa8fkd8pfpv2h9vs4vsacr7094",
    "DoNotUpdate": false,
    "EndEpochTime": 1680948000,
    "EventDescription": "Public holiday in Victoria",
    "EventName": "Good Friday (Victoria)",
    "ExpirationEpochTime": 1688205600,
    "Observed": true,
    "StartEpochTime": 1680861600,
    "CustomPrompt": "Sorry but we are closed today. Have a happy Good Friday!"
}
```

**Associating an event to specific queues**

To associate the event with only specific queues, add an attribute called ```AssociatedQueues``` with a array of strings representing the queues.

```
{
    "PKey": "Australia/Melbourne",
    "SKey": "20230407_oa8fkd8pfpv2h9vs4vsacr7094",
    "DoNotUpdate": false,
    "EndEpochTime": 1680948000,
    "EventDescription": "Public holiday in Victoria",
    "EventName": "Good Friday (Victoria)",
    "ExpirationEpochTime": 1688205600,
    "Observed": true,
    "StartEpochTime": 1680861600,
    "CustomPrompt": "Sorry but we are closed today. Have a happy Good Friday!",
    "AssociatedQueues": [ "Queue1", "Queue2" ]
}
```


## The Check Current Event Lambda

To use the Event Calendar in Amazon Connect contact flows, first add it to your Amazon Connect instance.
![Associate Lambda to Amazon Connect](imgs/associate-lambda-to-connect.png)

Once done, you can add the Lambda to contact flows.
![Add Lambda to Amazon Connect contact flows](imgs/add-lambda-to-contact-flow.png)

This Lambda requires the following parameters to be added:

| Parameter | Required | Description |
|---|---|---|
| Timezone | Yes | This is the timezone and must match the timezone(s) used during synchronisation. |
| QueueName | No | This is optional. If the name of the queue is provided, a result will only be returned when the event has been associated with the queue. |

The Lambda will return the following results:

| Key | Type | Description |
|---|---|---|
| Found | String | Whether and event was found. This will either be ```true``` or ```false```. |
| EventName | String | The name of the event, if found. |
| CustomPrompt | String | A custom prompt for the event if provided. If not provided, this returns ```none```. |


### Example

![Contact flow integration example](imgs/contact-flow-example.png)