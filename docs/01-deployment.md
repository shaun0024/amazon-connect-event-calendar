# DEPLOYMENT

## Pre-requisites

You will need to set up Google Cloud Platform (GCP) account to make use of the Google Calendar API. Generally speaking, there should be no cost to use this service. Refer to [this link](https://developers.google.com/calendar/api/guides/quota#:~:text=All%20use%20of%20the%20Google,available%20at%20no%20additional%20cost.).

**Get the Calendar IDs**
1. Access [Google Calendar](https://calendar.google.com) and add any public calendar(s) that are to be used.
2. Once added, access the settings for the calendar and take note of the calendar id
![Access public calendar settings](imgs/01-access-calendar-settings.png)
![Get public calendar id](imgs/02-get-calendar-id.png)

3. Repeat this for each calendar that needs to be synchronised.

**Get an API Key for Google Calendar**
1. Access the [GCP Console](https://console.cloud.google.com/) and create a new project (or use an existing one).
![Create new project](imgs/03-create-new-project.png)
![Provide project details](imgs/04-provide-project-details.png)

2. Once the project has been created, enable the Google Calendar API for the project.
![Access API library](imgs/05-enable-apis-for-project-1.png)
![Access API library](imgs/06-enable-apis-for-project-2.png)
![Search for Google Calendar API](imgs/07-search-for-google-calendar-api.png)
![Enable Google Calendar API](imgs/08-enable-google-calendar-api.png)

3. Create a new API key for the project and take note of it.
![Create a new API Key](imgs/09-create-new-api-key.png)
![Copy API key](imgs/10-copy-api-key.png)

4. Restrict the use of the API key to the Google Calendar API. You can also change the name of the API key.
![Restrict API key](imgs/11-restrict-api-key.png)

**Store the API Key in AWS Secrets Manager**
1. Access the [AWS Console](https://console.aws.amazon.com) and create a new secret in the Secrets Manager to hold the API key created earlier. The key must be named ```ApiKey```. Take note of the ARN of the newly created secret.
![Create new secret](imgs/12-create-secret.png)
![Take note of the ARN of the secret](imgs/13-get-secret-arn.png)



## Deploy the CloudFormation Stack
1. Review the parameters required for deployment in the table below.

| Parameter Name | Required? | Description |
|---|---|---|
| LoggingLevel | No | The logging level of the Lambda. Accepted values are ```INFO``` (default), ```ERROR```, ```WARNING```, ```DEBUG```. |
| SyncAheadWeeks | No | The number of weeks for future events to be synced. This is ```24``` by default. |
| ExpireAfterWeeks | No | The number of weeks to retain past events before automatic deletion. This is ```12``` by default |
| SecretArn | Yes | The ARN of the secret containing the API key created earlier. |
| CalendarMapName | Yes | A name for the AWS Systems Manager Parameter that will be created. This needs to be the fully qualified name (e.g. /name/of/parameter). |
| CalendarMap | Yes | A JSON formatted string containing the calendar ids and timezones. This needs to be in the format ```{"{calendar_id}":["timezone1","timezone2"]}``` (e.g. ```{"en.australian#holiday@group.v.calendar.google.com":["Australia/Melbourne","Australia/Sydney"],"en.malaysia#holiday@group.v.calendar.google.com":["Asia/Kuala_Lumpur"]}```). For a full list of timezone names that can be used, refer to this [link](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones). |

2. Once done, identify the region to be used and click on the link to launch the CloudFormation service page. Regions listed below are for those that support Amazon Connect.

| Region | Launch Link |
|---|---|
| US East (N. Virginia) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-1.console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |
| US West (Oregon) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |
| Asia Pacific (Seoul) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-2.console.aws.amazon.com/cloudformation/home?region=ap-northeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |
| Asia Pacific (Singapore) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-1.console.aws.amazon.com/cloudformation/home?region=ap-southeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |
| Asia Pacific (Sydney) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-2.console.aws.amazon.com/cloudformation/home?region=ap-southeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |
| Asia Pacific (Tokyo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-1.console.aws.amazon.com/cloudformation/home?region=ap-northeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |
| Canada (Central) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ca-central-1.console.aws.amazon.com/cloudformation/home?region=ca-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |
| Europe (Frankfurt) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |
| Europe (London) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/44539252/event-calendar.yaml&stackName=AmzConnectEventCalendar) |